#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <stddef.h>

enum FileTags {
	FileTagName = 0,
	FileTagFile = 1,
	FileTagTotalFiles = 2,
};

inline uint8_t read_u8(std::istream& is) {
	uint8_t o;
	is.read((char*)&o, 1);
	return o;
}

inline void write_u8(std::ostream& os, uint8_t v) {
	os.write((char*)&v, 1);
}

template <typename UInt>
UInt read_uint_le(std::istream& is) {
	UInt o = (UInt)0u;
	size_t const N = sizeof(UInt);
	uint8_t b[N];
	is.read((char*)b, N);
	for (size_t i = 0; i < N; ++i) {
		o |= (UInt)b[i] << (i*8);
	}
	return o;
}

template <typename UInt>
void write_uint_le(std::ostream& os, UInt v) {
	os.write((char*)&v, sizeof(UInt));
}

inline uint32_t read_u32_le(std::istream& is) {
	return read_uint_le<uint32_t>(is);
}

inline void write_u32_le(std::ostream& os, uint32_t v) {
	write_uint_le(os, v);
}

inline uint64_t read_u64_le(std::istream& is) {
	return read_uint_le<uint64_t>(is);
}

inline void write_u64_le(std::ostream& os, uint64_t v) {
	write_uint_le(os, v);
}

inline std::string read_sized_string(std::istream& is) {
	uint32_t cb = read_u32_le(is);
	if (cb == 0)
		return "";
	std::vector<char> b(cb);
	is.read(&b[0], cb);
	return std::string(b.begin(), b.end());
}

inline void write_sized_string(std::ostream& os, std::string const& s) {
	uint32_t cb = s.size();
	write_u32_le(os, cb);
	os.write(s.c_str(), cb);
}
