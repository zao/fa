# Building #
    mkdir build
    cd build
    cmake ..

# Usage #

* `fa-info a0.comp`
* `fa-extract a0.comp`
* `fa-archive a.comp filename...`

# File format #
The file format has a leading header followed by interleaved file headers and file data.

Header:

    +--------------------+ +---------------+                               
    |U8 FileCountTag 0x00| |U32LE FileCount|                               
    +--------------------+ +---------------+                               

File entry:

    +--------------------+ +-----------------+
    |U8 NameTag 0x01     | |U32LE NameLength |
    +--------------------+ +-----------------+
    +----------------------------------------+
    |U8[NameLength] Filename                 |
    +----------------------------------------+
                                              
    +--------------------+ +-----------------+
    |U8 FileTag 0x02     | |U64LE FileLength |
    +--------------------+ +-----------------+
    +----------------------------------------+
    |U8[FileLength] FileData                 |
    +----------------------------------------+
