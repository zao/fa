#include "byte_io.h"
#include <cassert>
#include <fstream>

int main(int argc, char** argv) {
	if (argc != 2) {
		return 1;
	}

	std::string archive_filename = argv[1];
	std::ifstream is(archive_filename.c_str(), std::ios::binary);
	// Read archive header
	uint8_t const file_count_tag = read_u8(is);
	assert(file_count_tag == FileTagTotalFiles);
	uint32_t const num_files = read_u32_le(is);
	printf("File count: %u\n", num_files);

	for (uint32_t i = 0; i < num_files; ++i) {
		// Read file entry
		uint8_t name_type_tag = read_u8(is);
		assert(name_type_tag == FileTagName);
		std::string filename = read_sized_string(is);
		uint8_t file_type_tag = read_u8(is);
		assert(file_type_tag == FileTagFile);
		uint64_t file_size = read_u64_le(is);
		is.seekg(file_size, std::ios::cur);
		printf("  %llu bytes, \"%s\"\n",
				(unsigned long long)file_size,
				filename.c_str());
	}
	// verify that the stream is at the end
	assert(is);
	is.get();
	assert(!is);
}
