#include "byte_io.h"
#include <algorithm>
#include <cassert>
#include <fstream>

#include <sys/types.h>
#include <sys/stat.h>
#if !defined(_MSC_VER)
#include <unistd.h>
#endif

#if defined(_MSC_VER)
typedef struct _stat64 stat_struct;
int stat_func(char const* filename, stat_struct* s) {
	return _stat64(filename, s);
}
#else
typedef struct stat stat_struct;
int stat_func(char const* filename, stat_struct* s) {
	return stat(filename, s);
}
#endif

uint64_t find_file_size(char const* filename) {
	stat_struct s;
	if (stat_func(filename, &s) < 0)
		return 0;
	return (uint64_t)s.st_size;
}

bool file_exists(char const* filename) {
	stat_struct s;
	if (stat_func(filename, &s) < 0)
		return false;
	return !!(s.st_mode & S_IFREG);
}

int main(int argc, char** argv) {
	if (argc <= 2)
		return 1;
	std::vector<std::string> filenames(argv + 2, argv + argc);
	for (size_t file_idx = 0; file_idx < filenames.size(); ++file_idx) {
		if (!file_exists(filenames[file_idx].c_str())) {
			return 2;
		}
	}
	std::string archive_filename = argv[1];
	uint32_t file_count = filenames.size();
	std::ofstream os(archive_filename.c_str(), std::ios::trunc |
			std::ios::binary);
	write_u8(os, FileTagTotalFiles);
	write_u32_le(os, file_count);
	for (size_t file_idx = 0; file_idx < filenames.size(); ++file_idx) {
		std::string filename = filenames[file_idx];
		write_u8(os, FileTagName);
		write_sized_string(os, filename);
		write_u8(os, FileTagFile);
		uint64_t file_size = find_file_size(filename.c_str());
		write_u64_le(os, file_size);
		std::ifstream is(filename.c_str(), std::ios::binary);
		uint64_t file_left = file_size;
		while (file_left) {
			uint8_t buf[64];
			size_t n = (size_t)std::min<uint64_t>(64,
					file_left);
			is.read((char*)buf, n);
			os.write((char*)buf, n);
			file_left -= n;
		}
		// verify that the stream is at the end
		assert(is);
		is.get();
		assert(!is);
	}
}
